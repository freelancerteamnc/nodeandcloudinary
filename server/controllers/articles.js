'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Article = mongoose.model('Article'),
    config = require('../config/config'),
    cloudinary = require('cloudinary'),
    _ = require('lodash');

// Configure cloudinary
cloudinary.config(config.cloudinary);

/**
 * Find article by id
 */
exports.article = function(req, res, next, id) {
    Article.load(id, function(err, article) {
        if (err) return next(err);
        if (!article) return next(new Error('Failed to load article ' + id));
        req.article = article;
        next();
    });
};

/**
 * Get signature for upload
 */
exports.getUploadAttrs = function(req, res, next) {
  var timestamp = new Date().getTime();
  var optionsCloudinary = cloudinary.utils.sign_request({timestamp: timestamp}, config.cloudinary); 
  optionsCloudinary.cloud_name = config.cloudinary.cloud_name;
  
  res.jsonp(optionsCloudinary);
};

/**
 * Create an article
 */
exports.create = function(req, res) {
    var article = new Article(req.body);
    article.user = req.user;

    article.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                article: article
            });
        } else {
            res.jsonp(article);
        }
    });
};

/**
 * Update an article
 */
exports.update = function(req, res) {
    var article = req.article;

    article = _.extend(article, req.body);

    article.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                article: article
            });
        } else {
            res.jsonp(article);
        }
    });
};

/**
 * Delete an article
 */
exports.destroy = function(req, res) {
    var article = req.article;

    article.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                article: article
            });
        } else {
            res.jsonp(article);
        }
    });
};

/**
 * Delete an image
 */
exports.destroyImage = function(req, res) {
    var imageId = req.params.imageId;
    var articleId = req.params.articleId;

    Article.update({_id: articleId}, {$pull: {images: imageId} }, function(err, resp){

      if (err) {
          res.render('error', {
              status: 500
          });
      } else {
          res.jsonp(resp);
      }

    });
};

/**
 * Show an article
 */
exports.show = function(req, res) {
    res.jsonp(req.article);
};

/**
 * List of Articles
 */
exports.all = function(req, res) {
    Article.find().sort('-created').populate('user', 'name username').exec(function(err, articles) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(articles);
        }
    });
};
