'use strict';

var mean = require('meanio'),
    config = require('../config/config');

exports.render = function(req, res) {

	var modules = [];

	// Preparing angular modules list with dependencies

	for (var name in mean.modules) {
		modules.push({
			name: name,
			module: 'mean.' + name,
			angularDependencies: mean.modules[name].angularDependencies
		});
	}

	// Send some basic starting info to the view
	res.render('index', {
		user: req.user ? JSON.stringify(req.user) : 'null',
		roles: req.user ? JSON.stringify(req.user.roles) : JSON.stringify(['annonymous']),
		api_key: config.cloudinary.api_key,
		cloud_name: config.cloudinary.cloud_name,
		modules: JSON.stringify(modules)
	});
};
