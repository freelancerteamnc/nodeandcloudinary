'use strict';

angular.module('mean.articles').controller('ArticlesController', ['$scope', '$stateParams', '$location', 'Global', 'Articles', 'ArticleImages', 'Cloudinary',
  function ($scope, $stateParams, $location, Global, Articles, ArticleImages, Cloudinary) {
    $scope.global = Global;

    // Carousel and images options
    $scope.widthCarousel = 800;
    $scope.myInterval = 3000;
    $scope.maxUpload = 10;
    $scope.uploading = false;
    //====================

    // Get cloudinary signature from back-end
    Cloudinary.getUploadAttrs('tag', function(data) {
      $scope.cloudinaryData = {
        url: 'https://api.cloudinary.com/v1_1/'+data.cloud_name+'/auto/upload',
        formData : {
           timestamp : data.timestamp,
           api_key : data.api_key,
           signature : data.signature
        },
        start : function() {
          $scope.uploading = true;
        },
        progress : function(e) {
          $scope.progress = (e.loaded * 100)/e.total; 
        },
        done : function(e, response) {
          $scope.uploading = false;
          $scope.addImage(response.result.public_id);
          $scope.progress = 0;
        }
      }
    });

    $scope.addImage = function(image){
      if(!angular.isArray($scope.article.images)) $scope.article.images = [];
      $scope.article.images.push(image);
    };

    $scope.create = function() {
        var article = new Articles({
            title: this.title,
            content: this.content
        });
        article.$save(function(response) {
            $location.path('articles/' + response._id);
        });

        this.title = '';
        this.content = '';
    };


    $scope.removeImage = function(image) {
      ArticleImages.remove({articleId: $scope.article._id, imageId: image});
      var index = $scope.article.images.indexOf(image);
      if(index !== -1) $scope.article.images.splice(index, 1);
    }

    $scope.remove = function(article) {
        if (article) {
            article.$remove();

            for (var i in $scope.articles) {
                if ($scope.articles[i] === article) {
                    $scope.articles.splice(i, 1);
                }
            }
        }
        else {
            $scope.article.$remove();
            $location.path('articles');
        }
    };

    $scope.update = function() {
        var article = $scope.article;
        if (!article.updated) {
            article.updated = [];
        }
        article.updated.push(new Date().getTime());

        article.$update(function() {
            $location.path('articles/' + article._id);
        });
    };

    $scope.find = function() {
        Articles.query(function(articles) {
            $scope.articles = articles;
        });
    };

    $scope.findOne = function() {
        Articles.get({
            articleId: $stateParams.articleId
        }, function(article) {
            $scope.article = article;
        });
    };
}]);
