'use strict';

//Articles service used for articles REST endpoint
angular.module('mean.articles')
.factory('Articles', ['$resource', function($resource) {
  return $resource('articles/:articleId', {
      articleId: '@_id'
  }, {
      update: {
          method: 'PUT'
      }
  });
}])
.factory('ArticleImages', ['$resource', function($resource) {
  return $resource('articles/:articleId/image/:imageId', {
      articleId: '@_id',
      imageId: '@imageId',
  }, {
      remove: {
          method: 'DELETE'
      }
  });
}]);
