angular.module('mean.system').factory('Cloudinary', [ '$http', function($http) {
  return {
    'getUploadAttrs' : function(tags, cb) {
    $http.get('/api/cloudinary', {
      params : {
        'tstamp' : new Date().getTime(),
        'tags' : tags
      }})
      .success(function(data) {
          cb(data);
      }).
      error(function(data, status, headers, config) {
        console.log(status + " | bad");
      });
  }}
}]);
